class Joueur{

    constructor(couleur, nomJoueur){
        this.couleur = couleur;
        this.nomJoueur = nomJoueur;
        this.nombreTerritoire = 0;
    }

    get Couleur(){
        return this.couleur;
    }

    get NomJoueur(){
        return this.nomJoueur;
    }

    equals(Joueur){
        return this.nomJoueur === Joueur.nomJoueur
    }

}